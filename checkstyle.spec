Name:                checkstyle
Version:             8.0
Release:             2
Summary:             Java source code checker
License:             LGPLv2+ and GPLv2+ and BSD
URL:                 http://checkstyle.sourceforge.net/
Source0:             http://download.sf.net/checkstyle/checkstyle-%{version}-src.tar.gz
Source2:             %{name}.catalog
Patch0:              0001-Do-not-load-external-DTDs-by-default.patch
Patch1:              checkstyle-8.0-guava.patch
BuildArch:           noarch
BuildRequires:       maven-local mvn(antlr:antlr) mvn(com.google.guava:guava)
BuildRequires:       mvn(commons-beanutils:commons-beanutils) mvn(commons-cli:commons-cli)
BuildRequires:       mvn(com.sun:tools) mvn(org.antlr:antlr4-maven-plugin)
BuildRequires:       mvn(org.antlr:antlr4-runtime) mvn(org.apache.ant:ant)
BuildRequires:       mvn(org.apache.ant:ant-nodeps)
BuildRequires:       mvn(org.apache.maven.plugins:maven-antrun-plugin)
BuildRequires:       mvn(org.apache.maven.plugins:maven-failsafe-plugin)
BuildRequires:       mvn(org.codehaus.mojo:antlr-maven-plugin)
BuildRequires:       mvn(org.codehaus.mojo:build-helper-maven-plugin)
Requires:            javapackages-tools
Obsoletes:           %{name}-optional < %{version}-%{release}
Obsoletes:           %{name}-demo < %{version}-%{release}
Obsoletes:           %{name}-manual < %{version}-%{release}
%description
A tool for checking Java source code for adherence to a set of rules.

%package        javadoc
Summary:             Javadoc for %{name}
%description    javadoc
API documentation for %{name}.

%prep
%setup -q -n %{name}-%{version}
%patch0 -p1
%patch1 -p1
%pom_remove_parent
sed -i s/guava-jdk5/guava/ pom.xml
%pom_remove_plugin :maven-eclipse-plugin
%pom_remove_plugin :maven-site-plugin
%pom_remove_plugin :nexus-staging-maven-plugin
%pom_remove_plugin :maven-enforcer-plugin
%pom_remove_plugin :cobertura-maven-plugin
%pom_remove_plugin :maven-linkcheck-plugin
%pom_remove_plugin :maven-pmd-plugin
%pom_remove_plugin :findbugs-maven-plugin
%pom_remove_plugin :xml-maven-plugin
%pom_remove_plugin :forbiddenapis
%pom_remove_plugin :spotbugs-maven-plugin
%pom_remove_dep com.sun:tools
%pom_add_dep com.sun:tools
sed -i 's/\r//' LICENSE LICENSE.apache20 README.md
sed -i '/testLoadFromURL/s/ *.*/    @org.junit.Ignore&/' src/test/java/com/puppycrawl/tools/checkstyle/filters/SuppressionsLoaderTest.java
sed -i '/testUnexpectedChar/s/./@org.junit.Ignore/' src/test/java/com/puppycrawl/tools/checkstyle/grammars/GeneratedJava14LexerTest.java

%build
%mvn_file  : %{name}
%mvn_build -f

%install
%mvn_install
%jpackage_script com.puppycrawl.tools.checkstyle.Main "" "" checkstyle:antlr:apache-commons-beanutils:apache-commons-cli:apache-commons-logging:apache-commons-collections:guava checkstyle true
install -Dm 644 %{SOURCE2} %{buildroot}%{_datadir}/xml/%{name}/catalog
cp -pa src/main/resources/com/puppycrawl/tools/checkstyle/*.dtd \
  %{buildroot}%{_datadir}/xml/%{name}
install -dm 755  %{buildroot}%{_sysconfdir}/ant.d
cat > %{buildroot}%{_sysconfdir}/ant.d/%{name} << EOF
checkstyle antlr apache-commons-beanutils apache-commons-cli apache-commons-logging guava
EOF

%post
if [ -x %{_bindir}/install-catalog -a -d %{_sysconfdir}/sgml ]; then
  %{_bindir}/install-catalog --add \
    %{_sysconfdir}/sgml/%{name}-%{version}-%{release}.cat \
    %{_datadir}/xml/%{name}/catalog > /dev/null || :
fi

%postun
if [ -x %{_bindir}/install-catalog -a -d %{_sysconfdir}/sgml ]; then
  %{_bindir}/install-catalog --remove \
    %{_sysconfdir}/sgml/%{name}-%{version}-%{release}.cat \
    %{_datadir}/xml/%{name}/catalog > /dev/null || :
fi

%files -f .mfiles
%license LICENSE
%doc README.md
%{_datadir}/xml/%{name}
%{_bindir}/%{name}
%config(noreplace) %{_sysconfdir}/ant.d/%{name}

%files javadoc -f .mfiles-javadoc
%license LICENSE

%changelog
* Mon Nov 13 2023 wangkai <13474090681@163.com> - 8.0-2
- Fix build failure caused by guava upgrade

* Thu Jul 30 2020 shaoqiang kang <kangshaoqiang1@huawei.com> - 8.0-1
- Package init
